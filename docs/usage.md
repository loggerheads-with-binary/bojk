
>  BOJK, or Brian on Jillian Kernel now has no Kernel. In fact the name stands only for historic reasons.


Brian is a directory search/aliasing functionality inspired by roscd from the Robot Operating System. 
It aliases directories through relative and absolute shorthands and can be used to search directories within a folder as well. 

Brian4 uses Chaeyoung to store cross-system paths. To enable this functionality, enable c-trans binary on your PATH variable. If it cannot find a c-trans translator, it automatically defaults to assuming the paths as-is. 

```python
Chaeyoungize = lambda x : x 
```

This would also mean the filesystem paths in the configuration file need to be changed to regular posix

Associated Filetypes:

    *.bojk :* Cache Files -> Pickle files containing cache data for search engine functionality 

    *.jillian :* YAML File containing configuration for the program 

> First fill the a *.jillian* file with all the necessary details, including Abspaths (Absolute paths to search for), Relpaths(Relative Paths to search for), Defpath(Top directory for relpaths), Command Failsafe(If similar paths must be suggested in case of incorrect command), Batfile(Shell/Batch file to write this information onto), Cache(A Path to a cache file) and Return_Index(No of files to be returned by the Search Engine Functionality)



Usage

- Commands:

```powershell
$ bojk -y /path/to/yaml.jillian -c <command>
$ /path/to/batfile 
```

This will search for the given `<command>` inside the abspaths, followed by relpaths. It will return the first result, and will execute the corresponding action(pushing the directory onto the dirstack by default)

- Search Engine

```powershell
$ bojk -y /path/to/yaml.jillian -s <search-term>
$ /path/to/batfile 

```

This will search for directories with the search term matching the basename of the directory. (Wildcards do not work here). For example "Riley" can be used as the search term for directories /whateverpath/riley1 /whatverpath/riley /whateverpath/rileydirectory etc. It returns the Return_Index most close searches for the user to choose from.

The --fps flag will search the entire relative path instead of just the basename. Ex 'Riley' could be used for even ./riley/nextpath and ./riley/55 etc, which could not have been achieved without the --fps flag 

- Config check

A form of lookup for the different confiuguration settings and directory aliases. 
The following positional arguments can be used for config check: 
    {'relpaths', 'codes', 'defpath', 'return-index', 'ctrl', 'shell-file', 'abspaths'}


- Generate
Generates a template yaml configuration file to be used later on. 


- Init Cache
Initializes the directory cache for later usage 


- Edit
Edit the configuration on command line. Using `brian -E` gets you to edit the configuration directly through ipython. 

- Investigate and Rectify
Investigate and rectify any possible errors in the YAML configuration 
