Name: Brian on Jillian Kernel [BOJK]
Version: 2.1.0

Summary:    Brian on Jillian Kernel or BOJK is a master program to change directories with multiple features
            [Try the argument `-u` to get more info on that]

    Arguments can be passed through using the terminal.
            A configuration file is necessary for proper use of BOJK.

Author: Aniruddh "Anna" Radhakrishnan
Author-email: aniruddh.iitb.92@protonmail.com
License: GNU GPL-3
Location: {path_}
Requires: brian_rapidfuzz, Scandir, Termtables, Argparse, Termcolor , Colorama, PyYAML

Associated Filetypes:
        Cache - Binary Pickle File : "*.bojk"
        Configuration - YAML File : "*.jillian"
