__author__: str
__license__: str
__version__: str

from brian_rapidfuzz import process, fuzz, utils, levenshtein, string_metric
