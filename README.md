## Introduction 

The program is supposed to allow a user to change/push/open directories based on assigned aliases. <br><br>
It was built as an indivividual project. None of this code is intended to be used as for production.

## Usage

First create a configuration file using `./brian.py -g` to generate a template configuration file in the cwd.    
Then fill the necessary information in the file 

To alias commands: `./brian.py -y /path/to/yaml.jillian -c <command>` <br> 
To alias searches: `./brian.py -y /path/to/yaml.jillian -s <search-term>` 

For more information, check `./brian.py -u` 

## Chaeyoung Integration 

Brian is compatible/extensible with Chaeyoung. To enable this, setup Chaeyoung and place the `c-trans` binary in the PATH environment variable. Once the script detects its presence, it will use c-trans for path conversions.<br> 
Note: If the program is extended with Chaeyoung, also remember to use Chaeyoung Header Paths in the configuration file 

## CLI Help 

```ruby 
usage: bojk [-h] [-c {command}] [-s {Search Term}] [-v] [-u] [-g [{Optional File}]] [-i] [-f [..]]
            [-o | --so | -S | -C | -P | --perr | -q | -p | --po] [-E] [--sbf | --uc] [--tc] [--fps] [-d DATAFILE]
            [--inspect] [--rectify] [--debug] [--cache-file CACHE] [--CI | --case-sensitive]

Show help:
  -h, --help            show this help message and exit


Basic Brian Options:
  -c , --command {command}
                        Use a command from the Configuration file.

  -s ,--search , --search-engine , --search-facility {Search Term}
                        Search for this directory in the defpath


More Options:

  -v, -V, --version     Show details about version of BOJKonCB

  -u, -U, --usage       Show usage of BOJKonCB

  -g , --generate , --touch , --create <filepath(Optional)>
                        Generates a blank new configuration file. If no path given, defaults to config.jillian

  -i, --init-cache      Initialize the BOJKonCB Cache feature which can be used later

  -f [..], --config [..], --ls [..], --list [..]
                        Show configuration data.

  -E, --edit, -e        BETA functionality: Edit the configuration. Uses ipython backend

  --inspect, --investigate
                        Inspects the configuration file for any imperfections
  --rectify, --correct-datafile
                        Rectifies a configuration file as far as possible by the program

Directory View/Obtain Options:
  -o, --open            Open directory in file explorer only

  --so, --shell-and-open
                        Open in the shell as well as the file explorer

  -S, --shell           Change directory in the shell

  -C, --copy            Copy the final path onto the clipboard

  -P, --print           Print the final path onto the terminal

  --perr, --print-on-error
                        Print the final path on stderr instead of stdout

  -R, --redis, --Redis  Push the final directory onto the `bojk` reserved variable on the Redis Server

  -p, --pushd           Push the directory on the shell stack, i.e. the `pushd` command

  --po, --pushd-and-open
                        Push the directory and open it in the explorer.

Search Modes[Flags]:

  --sbf, --search-by-file
                        Searches the directory based on the files inside it. NOTE CACHE FEATURE CANNOT BE USED FOR
                        THIS MODE
  --uc, --use-cache     Use Cache Instead of Walking the Folders in Realtime for the Search Engine Functionality

  --fps, --full-path-search
                        Forces the search engine to search for the entire path. Defaults to only directory name

Some More Internal Flag Options:

  --tc, --term-color    If flagged, the terminal prints a very colorful text.

  --debug, --debug-mode
                        Sets the logging level to `DEBUG` instead of `INFO`

  --CI, --case-insensitive
                        Enable Case Sensitivity of Searches.
  --case-sensitive, --CS
                        Disable Case Sensitivity of Searches.

Configuration Files:
 	-d <file>, -y <file>, --yaml <file>, --cfg <file>, --data <file>, --datafile <file>, --data-file <file>
                        Configuration python file with .jillian extension. No defaults for this.

    --cache-file <file>    Cache File to be used for BOJKonCB. Defaults to None
``` 